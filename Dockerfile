FROM registry.git.autistici.org/pipelines/images/base/debian:bookworm

RUN apt-get -q update && \
    env DEBIAN_FRONTEND=noninteractive apt-get -y install prometheus-alertmanager ca-certificates && \
    apt-get clean && \
    rm -fr /var/lib/apt/lists/*

ENTRYPOINT ["/usr/bin/prometheus-alertmanager"]

